-------------------------------------------------------------------------------
Ctools Plural String for Drupal 7.x
-------------------------------------------------------------------------------
Description:
The module provides a panel pane that displays plural string (including
count of items string).

An example of using the module:
- Display the number of comments
- Proper display of the calendar day

Note: Panel pane is in the miscellaneous tab.
