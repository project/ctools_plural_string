<?php

/**
 * @file
 * Primarily Drupal hooks to manipulate Ctools Plural String.
 *
 * This is the main module file for Ctools Plural String.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function ctools_plural_string_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'content_types') {
    return 'plugins/' . $plugin_type;
  }

  return NULL;
}

/**
 * Parsing parts links.
 *
 * @param string $url
 *   URL to parse.
 *
 * @return array
 *   Array of url pieces - only 'url', 'query', and 'fragment'.
 */
function _ctools_plural_string_parse_url($url) {
  $url_parts = array();

  // Separate out the anchor, if any.
  if (strpos($url, '#') !== FALSE) {
    $url_parts['fragment'] = substr($url, strpos($url, '#') + 1);
    $url = substr($url, 0, strpos($url, '#'));
  }

  // Separate out the query string, if any.
  if (strpos($url, '?') !== FALSE) {
    $query = substr($url, strpos($url, '?') + 1);
    $url_parts['query'] = _ctools_plural_string_parse_str($query);
    $url = substr($url, 0, strpos($url, '?'));
  }

  $url_parts['url'] = $url;
  return $url_parts;
}

/**
 * Replaces the PHP parse_str() function.
 *
 * Because parse_str replaces the following characters in query parameters name:
 *
 *   - chr(32) ( ) (space)
 *   - chr(46) (.) (dot)
 *   - chr(91) ([) (open square bracket)
 *   - chr(128) - chr(159) (various)
 *
 * @param string $query
 *   Query to parse.
 *
 * @return array
 *   Array of query parameters.
 *
 * @see http://php.net/manual/en/language.variables.external.php#81080
 */
function _ctools_plural_string_parse_str($query) {
  $query_array = array();
  $pairs = explode('&', $query);

  foreach ($pairs as $pair) {
    $name_value = explode('=', $pair, 2);
    $name = urldecode($name_value[0]);
    $value = isset($name_value[1]) ? urldecode($name_value[1]) : NULL;
    $query_array[$name] = $value;
  }

  return $query_array;
}
